# Canvas #
This project compose of seven different labs and each lab trains on a seperate topic. The project is aimed to step by step build a canvas where a user can draw different shapes.

Assignment 1 focuses implementing the different classes while training on Classes, interfaces, inheritance and polymorphism.

Assignment 2 focuses focuses on the Collection framework and how to compare objects.

Assignment 3 Focuses on exceptions handeling. 

Assignment 4 Focues on the graphical user interface Swing and its components. 

Assignment 5 Focueses on event handeling and creating a responsive GUI. 

Assignment 6 Explore some classes in Java 2D API and draw shapes using the different types of 2D-graphic 

Assignment 7 Focuses on Klassdiagrams, File handling and Lamda expressions