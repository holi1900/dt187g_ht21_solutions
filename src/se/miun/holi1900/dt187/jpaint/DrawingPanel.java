package se.miun.holi1900.dt187.jpaint;

import javax.swing.JPanel;

import java.awt.*;
import java.util.List;

/**
* <h1>DrawingPanel</h1>
* This Class takes a Drawing object as instantvariable and draws shapes on to a JPanel 
* <p>
* 
*
* @author Lima Honorine (holi1900)
* @version 1.0
*/

public class DrawingPanel extends JPanel{

    private Drawing drawing;

    public DrawingPanel() {
        this.drawing = new Drawing();
    }

    public DrawingPanel(Drawing drawing) {
        this.drawing = drawing;
    }

    
    public void setDrawing(Drawing drawing){
        this.drawing = drawing;
        repaint();
    }
    /**
     * sets name of the drawing
     * @param name
     */
    public void setDrawingName(String name){
        drawing.setName(name);
    }
    /**
     * sets author of the drawing
     * @param author
     */
    public void setDrawingAuthor(String author){
        drawing.setAuthor(author);
    }
    /**
     * adds shapeto drawing
     * @param shape
     */
    public void addShape(Shape shape){
        if(!drawing.addShape(shape)){
            System.err.println("The shape to be added to drawing is undefined.");
        }
    }

    /**
     * removelast added shape to drawing
     */
    public void removeLastShape(){
        if(!drawing.popShape()){
            System.err.println("Shape removal failed. Drawing do not contain any shapes.");
        }
        repaint();
    }

    /**
     * returns information about drawings; name, author, nr of shapes, total area and total circumference
     * @return
     */
    public String drawingInformation(){
        return drawing.drawingInformation();
    }
    /**
     * Adds list of shapes fron another drawing object to the class drawing object
     * @param newDrawing
     */
    public void addShapesFromDrawing(Drawing newDrawing){
        List<Shape> shapes = newDrawing.getShapes();

        for(int i=0; i<shapes.size(); i++){
            drawing.addShape(shapes.get(i));
        }
    }
    /**
     * 
     * @return drawing
     */
    public Drawing getDrawing(){
        return drawing;
    }

    /**
     * if drawing has name and author, return string in format "name" by "author" 
     * if only name or only author return name or author respectively
     * if none is given return empty string
     * @return 
     */
    public String getNameAndAuthor(){
        return this.drawing.getNameAndAuthor();
    }
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawing.draw(g);
    }
}
