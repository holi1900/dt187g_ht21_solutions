package se.miun.holi1900.dt187.jpaint;

/**
* <h1>Drawing</h1>
* This Class handles the drawing of a list of shapes. This class has data members for the name of drawing 
* collection, author, and a List of shapes
a constructor is defined that takes name and author as argurments
* This class possess methods to get and set name and authour, add shape to list, get size of list, get totol areaa
* and total circumference, draw shapes and get information about drawing as a string.
*  Method to save to file.
* 
* 
* 
* 
* <p>
* 
*
* @author Lima Honorine (holi1900)
* @version 1.0
*/
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.nio.file.*;
import java.io.File;
import java.io.FileNotFoundException;
public class Drawing implements Drawable{
    private String name = "";
    private String author = "";
    private List<Shape> shapes = new ArrayList<Shape>();

    public Drawing() {
    }

    public Drawing(String name, String author) {
        this.name = name;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name==null)
        {
            this.name = "";
        }
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        if(author==null)
        {
            this.author = "";
        }
        this.author = author;
    }

    public List<Shape> getShapes() {
        return shapes;
    }

    /**
     * Function receives reference to a Shape object and controlls that it does not refers to null
     * then add shape to List
     * @param shape
     * @return boolean true if shape was successfull added otherwise false
     */
    public  boolean addShape(Shape shape){
        if(shape==null){
            return false;
        }else{
            return this.shapes.add(shape);
        }
        
    }
    /**
     * removes last shape on the list
     * @return
     */
    public boolean popShape(){
        if(shapes.isEmpty()){
            return false;
        }
        else{
            int lastIndex = shapes.size() - 1;
            shapes.remove(shapes.get(lastIndex));
            return true;
        }
    }
    /**
     * if drawing has name and author, return string in format "name" by "author" 
     * if only name or only author return name or author respectively
     * if none is given return empty string
     * @return 
     */
    public String getNameAndAuthor(){
        String info = "";
        if(!name.isBlank() && !author.isBlank()){
            info = name + " by " + author;
        }
        else if(!name.isBlank() && author.isBlank()){
            info = name;
        }
        else if(name.isBlank() && !author.isBlank()){
            info = author;
        }
        return info;
    }

    /**
     * returns information about drawings; name, author, nr of shapes, total area and total circumference
     * @return
     */
    public String drawingInformation(){
        String info = getNameAndAuthor();

        info = info + "\nNumber of shapes: " + shapes.size() + "\nTotal area: " + getTotalArea() + "\nTotal Circunference: " + getTotalCircumference();

        return info;
    }

    /**
     * 
     * @return size of the list of shapes as an int
     */
    public int getSize(){
        return shapes.size();
    }

    /**
     * adds up the circumference of all the shapes in the list which has a circumference, i.e not =-1
     * @return sum as a double
     */
    public double getTotalCircumference(){
        double sum = 0.0;
        for(int i=0; i<shapes.size(); i++){
            if(shapes.get(i).getCircumference()!=-1)
            {
                sum += shapes.get(i).getCircumference();
            }
        }
        return sum;

    }

    /**
     * adds up the Area of all the shapes in the list which has a Area, i.e not =-1
     * @return sum as a double
     */
    public double getTotalArea(){
        double sum = 0;
        for(int i=0; i<shapes.size(); i++){
            if(shapes.get(i).getArea()!=-1)
            {
                sum += shapes.get(i).getArea();
            }
        }
        return sum;

    }

    
    public String toString(){
        return "name=" + name + "; author=" + author + "; size=" + shapes.size()
                + "; circumference=" + getTotalCircumference() + "; area=" + getTotalArea();
    }
    
    /**
     * Function throws an exception if name or author is missing otherwise saves drawing to a file using the filename received as a parameter 
     * @param filename 
     * @return true if file is saved
     * @throws DrawingException if file cannot be saved because author or name is missing
     */
    public boolean save(String filename) throws DrawingException{
        if(name==null){
            name = "";
        }
        if(author==null){
            author = "";
        }

        if(this.name.isEmpty() && this.author.isEmpty()){
            throw new DrawingException("The drawing name and author name is missing");
        }
        else if(this.author.isEmpty()){
            throw new DrawingException("The drawing name is missing");
        }else if(this.name.isEmpty()){
            throw new DrawingException("The drawing author is missing");
        }else{
            Path path = Path.of(filename);
            if(!path.toString().toLowerCase().endsWith(".shape")){
                filename += ".shape";
                path = Path.of(filename);
            }
            
            String data = name + "\n" + author + "\n";
            for(int i= 0; i<shapes.size(); i++){
                data += shapes.get(i).getsaveInfo();    
            }
            try{
                Files.writeString(path, data, StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                return true;
            }
            catch (Exception e){
                System.err.println(e.getMessage());
                return false;
            }            
        }
    }

    
    /**
     * reads drawing objects from file and load to this
     * @param filename
     * @return
     */
    public boolean load(String filename){
        try{
            Scanner fileScanner = new Scanner(new File(filename));
            this.name = fileScanner.nextLine();
            this.author = fileScanner.nextLine();
            while(fileScanner.hasNextLine()){
                Scanner lineScanner = new Scanner(fileScanner.nextLine()).useDelimiter(",");
                lineScanner.useLocale(Locale.US);
                String type = lineScanner.next();
                double startPointX = lineScanner.nextDouble();
                double startPointY = lineScanner.nextDouble();
                double endPointX = lineScanner.nextDouble();
                double endPointY = lineScanner.nextDouble();
                String color = lineScanner.next();
                if(type.equalsIgnoreCase("circle")){
                    Circle circle = new Circle(startPointX, startPointY, color);
                    circle.addPoint(endPointX, endPointY);
                    addShape(circle);
                }
                if(type.equalsIgnoreCase("rectangle")){
                    Rectangle rectangle = new Rectangle(startPointX, startPointY, color);
                    rectangle.addPoint(endPointX, endPointY);
                    addShape(rectangle);
                }
            }
            fileScanner.close();
            return true;
        }
        catch(FileNotFoundException e){
            return false;
        }
    }

    @Override
    public void draw() {
        System.out.println("A drawing by " + author + " called " + name);
        for(int i = 0; i<shapes.size(); i++ ){
            System.out.println(shapes.get(i).toString());
        }
    }

    @Override
    public void draw(Graphics g) {
        if(!shapes.isEmpty()){
            for(int i=0; i<shapes.size(); i++){
                shapes.get(i).draw(g);
            }
        }
        else{
            System.err.println("No shapes availble for printing.");
        }
    }
    
}
