package se.miun.holi1900.dt187.jpaint;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
* <h1>Assignment 4</h1>
* This class is the starting point for the JPaint drawing application.
* It creates a JFrame and makes it visible.
* 
* @author  Lima Honorine (holi1900)
* @version 1.0
*/
public class Assignment4 {

	public static void main(String[] args) {
		try{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		// Always make sure GUI is created on the event dispatching thread.
		// This will be explained in Java III (the lesson about threads).
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JPaintFrame().setVisible(true);
			}
		});		
	}
}
