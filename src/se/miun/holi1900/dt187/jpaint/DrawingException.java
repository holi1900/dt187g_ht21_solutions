package se.miun.holi1900.dt187.jpaint;

public class DrawingException extends Exception{

    public DrawingException() {
        super();
    }

    public DrawingException(String message) {
        super(message);
    }
    
}
