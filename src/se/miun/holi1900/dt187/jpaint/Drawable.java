package se.miun.holi1900.dt187.jpaint;

/**
* <h1>Drawable</h1>
* This class is an interface for drawing
* two abstract method for draw
* <p>
* 
*
* @author Lima Honorine (holi1900)
* @version 1.0
*/
public interface Drawable {
    void draw();
    void draw(java.awt.Graphics g);
    
}
