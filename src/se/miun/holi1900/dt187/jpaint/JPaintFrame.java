package se.miun.holi1900.dt187.jpaint;
import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.plaf.DimensionUIResource;

import java.awt.*;
import java.awt.event.*;
import java.nio.channels.SelectableChannel;
import java.util.*;
import java.nio.file.*;


/**
* <h1>JPaintFrame.java</h1>
* This class manages the contents and properties of the components 
* in a JFrame where different shapes can be drawn.
* The JFrame has an icon, a name, and a menubar.
* The menubar has two menu options:
* File which has Items new, save as, load, and exit, and 
* Edit, with items undo, Name, and Author
* The new option starts new drwing and promts user for drawing name and author and update title.
* save option prompt user for filename and saves drwing to the given file
* load option prompt user for file to ne read and load drawing from given file
* Exit ends program
* Name and author options get name and author from user respectively and update title
* The rest of the JFrame is divide in a borderLayout.
* The START-PAGE contains a JPanel that contains a jPanel containing jpanels of different colors 
* and a JCombobox with different shape options.
* when clicked of each color, the color is selected and used for drawing.
* The shape options that a user can choose from to draw are Rectangle, Circle and create shape.
* The center contains a JPanel that will be used for drawing shapes
* The END_PAGE of the Borderlayout contains a Label alligned to the left that displays cordinates and 
* a Panel alligned to the right that displays selected color.
* The components of the JFrame are placed in a BorderLayout 
* 
*
*
*
* @author  Lima Honorine (holi1900)
* @version 1.0
*/
public class JPaintFrame extends JFrame implements ActionListener{

    private JToolBar toolbar; // Container for components to be placed in START_PAGE BorderLayout
    private DrawingPanel drawingArea; // Container placed in CENTER BorderLayout to contaiing drawing area
    private JPanel endPageBar; //Container for components to be placed in END_PAGE of JFrame BorderLayout
    private JPanel colorPanelContainer; //Container for the different color panels
    private java.util.List<JPanel> colorPanels = new ArrayList<>(); //List of all color JPanels
    private java.util.List<Color> colors = new ArrayList<>(); //List of all Color objects used to create Color JPanels
    private JComboBox<String> shapeOptions; //Holds different shape options user can choose from
    //private JPanel cordinatePanel;
    private JPanel selectedColorPanel; // Container for components to display selected color info in END_PAGE
    private Point cordinates; // holds coordinates to be displayed
    private JPanel selectedColor; // background color varies to display the selected color from toolbar
    private JLabel cordinateLabel; //displays cordinates as set text
    private JLabel selectedColorLabel; // display the text "Selected color: "
    private final String nameTxt = "Enter name of the drawing:";       
    private final String authorTxt = "Enter author of the drawing:";
    private final String saveTxt = "Save drawing to:";



    public JPaintFrame() {

        super("The Drawing Kit");

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setLocationRelativeTo(null);

        ArrayList<Image> iconImage = new ArrayList<Image>();
        iconImage.add(new ImageIcon("logo2.jpg").getImage());
        setIconImages(iconImage);

        // initializes components and set default values for the different components in the JFrame
        initComponents();

        setSize(750, 500);

        setVisible(true);

    }
    private void initComponents(){
        
        cordinates = new Point(0,0);

        //Create menu bar
        createMenuBar();

        // initaializes the different containers too be used and sets layout where needed.
        toolbar = new JToolBar();
        //toolbar.setLayout(new BoxLayout(toolbar, BoxLayout.X_AXIS));
        colorPanelContainer = new JPanel(new GridLayout(1,6));
        drawingArea= new DrawingPanel(defaultDrawing());
        updateTitle();
        drawingArea.setBackground(Color.WHITE);
        drawingArea.addMouseMotionListener(l2);
        endPageBar = new JPanel(new BorderLayout());
        selectedColorPanel = new JPanel();
        selectedColorPanel.setLayout(new BoxLayout(selectedColorPanel ,BoxLayout.X_AXIS));
        endPageBar = new JPanel();
        endPageBar.setLayout(new BoxLayout(endPageBar, BoxLayout.X_AXIS));
        shapeOptions = new JComboBox<>();

        //create a JPanel for each color and place all in a JPanel
        loadColorPanelContainer();
        
        //Create shape option JCcombox and fill options
        loadShapeOptions();
             
        toolbar.add(colorPanelContainer);
        toolbar.add(shapeOptions);

        //create and load Coordinate information;
        cordinateLabel = new JLabel();
        loadCoordinateInfo();
        //Create the panel displaying selected color information
        createSelectedColorPanel();

        //Add cordinateLabel and selectedColorPanel to container enPageBar JPanel
        endPageBar.add(cordinateLabel);
        cordinateLabel.setAlignmentX(0);
        endPageBar.add(Box.createHorizontalGlue());
        endPageBar.add(selectedColorPanel);
        selectedColorPanel.setAlignmentX(1);

        //Place components in the different positions in JFrame borderLayout
        setLayout(new BorderLayout());
        add(toolbar, BorderLayout.PAGE_START);
        add(drawingArea, BorderLayout.CENTER);
        add(endPageBar, BorderLayout.PAGE_END);    
    }

    MouseMotionListener l2 = new MouseMotionAdapter(){
        @Override
        public void mouseMoved(MouseEvent e) {
            cordinates.setX(e.getX());
            cordinates.setY(e.getY());
            loadCoordinateInfo();
        }
    };
    
    /**
     * fills color List with the different color objects to be used
     */
    private void setColors(){
        colors.add(Color.GREEN);
        colors.add(Color.BLUE);
        colors.add(Color.BLACK);
        colors.add(Color.RED);
        colors.add(Color.YELLOW);
        colors.add(Color.ORANGE);
    }
    
    /**
     * create JPanel for each color object in colors List and store them in the colorPanels List
     */
    private void setColorPanels(){
        setColors();
        for(int i=0; i<colors.size(); i++){
            JPanel panel = new JPanel();
            panel.setBackground(colors.get(i));
            panel.addMouseListener(l);
            colorPanels.add(panel);
        }
    }
    /**
     * Declaration for MouseListener used for color panel to listen for click.
     * when cilcked event occurs, the color that is clicked on is set as a selected color
     */
    MouseListener l = new MouseAdapter(){
        @Override
    public void mouseClicked(MouseEvent e) {
        for(int i=0; i<colorPanels.size(); i++){
            if(e.getComponent()== colorPanels.get(i)){
                selectedColor.setBackground(colors.get(i));
            }
        }
    }
    };
    
    /**
     * Place the color JPanels in the container
     */
    private void loadColorPanelContainer(){
        setColorPanels();
        for(int i=0; i<colorPanels.size(); i++){
            colorPanelContainer.add(colorPanels.get(i));
        }
    }

    /**
     * Creates menu bar and sets it on the JFrame
     */
    private void createMenuBar(){
        JMenuBar menu = new JMenuBar();
        //menu.setBorder(new LineBorder(Color.BLACK));
        JMenu file = new JMenu("File");
        JMenu edit =  new JMenu("Edit");

        JMenuItem newItem = new JMenuItem("New...");
        JMenuItem saveItem = new JMenuItem("Save as...");
        JMenuItem loadItem = new JMenuItem("Load...");
        JMenuItem exitItem = new JMenuItem("Exit");
        JMenuItem infoItem = new JMenuItem("Info");
        exitItem.setBorder(new LineBorder(Color.BLACK));

        // add actionListeners
        newItem.addActionListener(this);
        saveItem.addActionListener(this);
        loadItem.addActionListener(this);
        exitItem.addActionListener(this);
        infoItem.addActionListener(this);

        file.add(newItem);
        file.add(saveItem);
        file.add(loadItem);
        file.add(infoItem);
        file.addSeparator();
        file.add(exitItem);

        JMenuItem undoItem = new JMenuItem("Undo");
        JMenuItem nameItem =  new JMenuItem("Name...");
        JMenuItem authorItem =  new JMenuItem("Author...");

        undoItem.addActionListener(this);
        nameItem.addActionListener(this);
        authorItem.addActionListener(this);

        edit.add(undoItem);
        edit.add(nameItem);
        edit.add(authorItem);

        menu.add(file);
        menu.add(edit);

        setJMenuBar(menu); 
    }

    /**
     * creates JCombobox contaioning different shape options user can create
     */
    private void loadShapeOptions(){
        shapeOptions.addItem("Rectangle");
        shapeOptions.addItem("Circle");
        shapeOptions.addItem("Create shape");
        shapeOptions.setMaximumSize(shapeOptions.getPreferredSize());
        shapeOptions.setMinimumSize(shapeOptions.getPreferredSize());
    }

    /**
     * updates coordinates information with current cordinates
     */
    private void loadCoordinateInfo(){
        cordinateLabel.setText("Coordinates: " + cordinates.toString());
    }

    /**
     * sets properties of JPanel dispaying selected colors
     */
    private void createSelectedColorPanel(){
        selectedColor = new JPanel();
        selectedColor.setMaximumSize(selectedColor.getPreferredSize());
        selectedColor.setBackground(Color.GREEN);
        selectedColor.setPreferredSize(new DimensionUIResource(20, 20));
        selectedColorLabel = new JLabel("Selected color: ");
        selectedColorLabel.setLabelFor(selectedColor);
        selectedColorPanel.add(selectedColorLabel);
        selectedColorPanel.add(selectedColor);
    }

    /**
     * Updates the title of the drawing with drawing's name and author name
     */
    private void updateTitle(){
        String newTitle = "The Drawing Kit-" + drawingArea.getNameAndAuthor();
        setTitle(newTitle);
    }

    /**
     * Display a dialog box prompting user to enter Drawings name and 
     * assigning entered text to current drawing name variable drawingName
     */
    private void updateDrawingName(){
        String enteredText = JOptionPane.showInputDialog(nameTxt);
        String drawingName = "";
        if(enteredText != null && !enteredText.isEmpty()){
            drawingName = enteredText;
        }
        drawingArea.setDrawingName(drawingName);
        updateTitle();
    }

    /**
     * Display a dialog box prompting user to enter Drawings author and 
     * assigning entered text to current author variable drawingAuthor
     */
    private void updateDrawingAuthor(){
        String enteredText = JOptionPane.showInputDialog(authorTxt);
        String drawingAuthor = "";
        if(enteredText != null && !enteredText.isEmpty()){
            drawingAuthor = enteredText;
        }
        drawingArea.setDrawingAuthor(drawingAuthor);
        updateTitle();
        
    }

    /**
     * gets filename by using current drawing name and author
     * @return filename
     */
    private String getFilename(){
        return drawingArea.getDrawing().getName() + drawingArea.getDrawing().getAuthor() + ".shape";
    }

    /**
     * Display a dialog box prompting user to enter filename to which drawing should be saved to  
     * A suggested name is displayed which is gotten from getFilename function
     */
    private void saveDrawingToFile(){
        String suggested = getFilename();
        String filename = JOptionPane.showInputDialog(saveTxt,suggested);

        if(filename != null && !filename.isEmpty()){
            Drawing drawing = drawingArea.getDrawing();
            try{
                if(drawing.save(filename)){
                    String msg = "Your drawing was successfully saved.";
                    JOptionPane.showMessageDialog(drawingArea, msg, "Saved", JOptionPane.INFORMATION_MESSAGE);
                }
            }
            catch (DrawingException e){
                JOptionPane.showMessageDialog(drawingArea, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }            
        }
        else{
            JOptionPane.showMessageDialog(drawingArea, "Filename can not be empty", "Error", JOptionPane.ERROR_MESSAGE);
        }   
    }

    /**
     * Display a dialog box prompting user to filename for a file containing drawing the 
     * user needs to display
     */
    private void loadDrawingFromFile(){
        String filename = JOptionPane.showInputDialog("Load drawing from:");
        Drawing newDrawing = new Drawing();
        if(newDrawing.load(filename)){
            drawingArea.setDrawing(newDrawing);
            updateTitle();
        }   
        else{
            JOptionPane.showMessageDialog(drawingArea, "File not found.","Error", JOptionPane.ERROR_MESSAGE );
        }     
    }

    private void displayDrawingInfo(){
        String message = drawingArea.drawingInformation();
        JOptionPane.showMessageDialog(null, message);
    }

    /**
     * removes the last added shape in the drawing
     */
    private void removeLastShape(){
        drawingArea.removeLastShape();
    }

    /**
     * resets drawing to a empty drawing and clear drawing area
     */
    private void startNewDrawing(){
        drawingArea.setDrawing(new Drawing());
        updateDrawingName();
        updateDrawingAuthor();
        updateTitle();
        drawingArea.repaint();
    }

    /**
     * returns a drawing object containing five hard coded shape objects
     * @return
     */
    private Drawing defaultDrawing(){
        
        Drawing drawing = new Drawing("My shape collection", "Honorine");
        Circle c1 = new Circle(50, 20, "#0000ff");
        c1.addPoint(40, 30);
        drawing.addShape(c1);

        Circle c2 = new Circle(200, 0, "#000000");
        c2.addPoint(140, 40);
        drawing.addShape(c2);

        Rectangle r1 = new Rectangle(100, 20, "#123456");
        r1.addPoint(120, 80);
        drawing.addShape(r1);

        Rectangle r2 = new Rectangle(20, 100, "#FF0000");
        r2.addPoint(120, 180);
        drawing.addShape(r2);

        Circle c3 = new Circle(300, 100, "#FFFF00");
        c3.addPoint(240, 80);
        drawing.addShape(c3); 
        
        return drawing;
    }

   
    @Override
    public void actionPerformed(ActionEvent e) {

        switch (e.getActionCommand()){
            case "New...":
                startNewDrawing();
                break;
            case "Save as...":
                saveDrawingToFile();
                break; 
            case "Load...":
                loadDrawingFromFile();
                break;
            case "Info":
                displayDrawingInfo();
                break;
            case "Exit":
                System.exit(0);
                break;  
            case "Undo":
                removeLastShape();
                break;
            case "Name...":
                updateDrawingName();
                break;
            case "Author...":
                updateDrawingAuthor();
                break;
        }        
    }    
    
}
