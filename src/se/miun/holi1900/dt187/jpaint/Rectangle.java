package se.miun.holi1900.dt187.jpaint;

import java.awt.*;
import java.awt.geom.*;
/**
* <h1>Rectangle</h1>
* This Class manages the properties and behaviours of the shape type, rectangle.
* Rectangle class inherits from Shape class
* inherits two properties, color, and an Array of Point objects 
* This class has two constructors, one that receives color and a point object has parameter and 
* the second that recieves color and x and y values for a point.
* overwrites inherited methods to get circumference, get area, get information about rectangle as a String,
* to draw the shape to the standard output, draw Shape to a GUI implemented from Drawable
* Implements class methods to get height and get width
* <p>
* 
*
* @author Lima Honorine (holi1900)
* @version 1.0
*/

public class Rectangle extends Shape {

/**       Constructors   */
    public Rectangle(double x, double y, String color) {
        this(new Point(x, y), color);
    }

    public Rectangle(Point point, String color) {
        super(point, color);
        this.maxSize = 2;
    }

    /**
     * computes height of a rectangle if the endpoint of the rectangle has been added else returns -1.
     * height is absolut value of the diffence between the y values of the two points
     * @return
     */
    public double getHeight(){
        if(hasEndPoint())
        {
            
            return Math.abs(points.get(1).getY() - points.get(0).getY());
        }
        return -1;
    }

    /**
     * computes width of a rectangle if the endpoint of the rectangle has been added else returns -1.
     * width is absolut value of the diffence between the x values of the two points
     * @return
     */
    public double getWidth(){
        if(hasEndPoint())
        {
            return Math.abs(points.get(1).getX() - points.get(0).getX());
        }
        return -1;
    }

    /**
     * returns the points, width, height, colors of the rectangle as a single String
     */
    @Override
    public String toString(){
        String startPoint = points.get(0).toString();
        String color =  getColor();
        String endPoint, height, width;

        if(hasEndPoint())
        {
            endPoint = points.get(1).toString();
            width = String.valueOf(getWidth());
            height = String.valueOf(getHeight());
        }
        else{
            endPoint = "N/A";
            height = "N/A";
            width = "N/A";
        }
        
        return "Rectangle [start=" + startPoint + "; end=" + endPoint + "; width=" + width + "; height=" + height + "; color=" + color + "]";
    }

    /**
     * Computes the area of a rectangle as width * heighth and 
     * returns area as a double
     */
    @Override
    public double getArea() {
        
        if(hasEndPoint()){
            return getHeight()*getWidth();
        }
        return -1;
    }

    /**
     * Computes the surface area of a rectangle as (width + heighth)/2 and 
     * returns area as a double
     */
    @Override
    public double getCircumference() {

        if(hasEndPoint()){
            return (getHeight() + getWidth()) * 2;
        }
        return -1;
    }

    /**
     * returns a String data to be written to file when shape is to be saved to file
     */
    @Override
    public String getsaveInfo() {
        return "rectangle," + String.valueOf(points.get(0).getX())  + "," + String.valueOf(points.get(0).getY()) + "," 
        + String.valueOf(points.get(1).getX())  + "," + String.valueOf(points.get(1).getY()) + "," + this.getColor() + "\n";
    }

    /**
     * Prints message specifying details about rectagle being drawn
     */
    @Override
    public void draw() {
        
        System.out.println("Drawing a " + toString());
    }

    @Override
    public void draw(Graphics g) {
        if(hasEndPoint()){
            Graphics2D g2 = (Graphics2D)g;
            Point startPoint = points.get(0);
            int x = (int) startPoint.getX();
            int y = (int) startPoint.getY();
            int height = (int) getHeight();
            int width = (int) getWidth();

            g2.setPaint(Color.decode(this.getColor()));
            g2.fillRect(x, y, width, height);
        }
        else{
            System.out.println("Shape cannot be draw becuase enddpoint is missing.");
        }
    } 
}
